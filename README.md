# Lettre d'info Alternatiba Toulouse

Ceci est le dépôt des lettres d'information de Alternatiba Toulouse.

Ce dépôt utilise les pages statiques de gitlab pour afficher le contenu du dossier `public` qui contient toutes les lettres d'info envoyées sur toulouse-info.

Les lettres visibles _(ordre anti-chronologique)_ :

* [Défilé au Carnaval samedi 14 avril](http://alternatiba.frama.io/lettre-info-toulouse/lettre-info-carnaval-2018-0412.html) - 12 avril 2018
* [Bazar au Bazacle : APPEL A L’AIDE MILITANTE du 23 avril au 3 mai, jusqu’au bout de la convergence !!!](http://alternatiba.frama.io/lettre-info-toulouse/lettre-info-appel-benevoles-bazaraubazacle-2018-0410.html) - 9 avril 2018
